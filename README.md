# Omnitux

This release of Omnitux is a port from the latest version of the original project, available [here](https://omnitux.sourceforge.net/index.en.php). Please check out all info and contributors to the original project there.
This port was demanded and carried out by the [FUSS](https://fuss.bz.it/) team in order to continue using this educational game in schools.


## Installation

Install with pip as usual

```bash
$ pip install omnitux
```

and run with

```bash
$ omnitux
```

or check out packages available for Linux distributions.

## Making deb package

Install the necessary dependencies

```bash
$ sudo apt install python3-all python3-gi gobject-introspection gir1.2-gtk-4.0 dh-python python3-stdeb
```

and build the package with 

```bash
$ DEB_BUILD_OPTIONS=nocheck python setup.py --command-packages=stdeb.command bdist_deb
```

The generated files will be in the `deb_dist` folder.

## Changelog


#### v1.2.2 - 12/2023
- Porting of the game for newer (3.x) Python versions and release on PyPi


#### v1.2.1 - 10/2011
- Linux only release to fix various issues with recent Linux distributions (new Python, package managers)
- Some upcoming features of next versions are included (although most are not active yet)



#### v1.2.0 - 8/2010
- Italian translation
- replacement of media files with a 'Non Commercial' license => omnitux is now 100% 'free as in freedom'
- "light" package version for low bandwith / small performance devices (netbooks...)
- added "options" screen ("little rocket") to modify setup (screen size, music volume...)
- 4 new activities :
	- French monuments
	- Vehicles (mouse manipulation + memory cards + differences)
- usual bug fixes and improvements


#### v1.1.0 - 4/2010

- Spanish translation
- new type of activity : mouse manipulation to transform one image into another
- new activities :
	- image transformation : Photos of Animals, Landscapes, SVG drawings
	- guess first letter (French only)
	- Central America and Caribbean countries
- toggle music on/off with F1 key
- various bug fixes and improvements


#### v1.0.0 - 23/11/2009

- new activities : 
	- flags and locations of the European Union countries
	- musical scale
- new 'Earth from Moon' menu to choose activities
- use of 'tags' to classify activities
- traditional bug fixes and improvements


#### 0.9.0 - 18/9/2009

- Portuguese translation, 
- new type of activity (differences between pictures), 
- new activities (find letter, playing cards), 
- .deb package for easier install under Ubuntu, 
- usual various bug fixes and improvements.


#### v0.8.0 - 13/6/2009

- graphical improvements by Tutur
- difficulty levels in some activities
- new activity : 'animal sounds'
- various bug fixes and clean up.


#### v0.7.1 - 26/4/2009

- new activities, 
- German translations, 
- various bugfixes and improvements.


#### v0.6.0 - 28/2/2009

- third type of activity: "memory cards". 
- updated example activities
- bugfixes.
